package com.example.distributedsystems2021client;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.distributedsystems2021client.databinding.ActivityMainBinding;
import com.google.android.material.tabs.TabLayoutMediator;

public class MainActivity extends AppCompatActivity {
	
	String[] tabTitles;
	Client client;
	
	private ActivityMainBinding binding;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		client = Client.getSingletonInstance();
		
		binding = ActivityMainBinding.inflate(getLayoutInflater());
		setContentView(binding.getRoot());
		
		tabTitles = new String[]{
				getResources().getString(R.string.firstTabTitle),
				getResources().getString(R.string.secondTabTitle),
				getResources().getString(R.string.thirdTabTitle)
		};
		
		binding.pager.setAdapter(new HomeScreenTabAdapter(this));
		
		new TabLayoutMediator(binding.tabLayout, binding.pager, (tab, position) -> tab.setText(tabTitles[position])).attach();
	}
	
	
	// Inner classes
	
	private static class HomeScreenTabAdapter extends FragmentStateAdapter {
		
		public HomeScreenTabAdapter(@NonNull FragmentActivity fragmentActivity) {
			super(fragmentActivity);
		}
		
		@NonNull
		@Override
		public Fragment createFragment(int position) {
			Fragment fragment;
			switch(position) {
				case 0:
					fragment = new FeedFragment();
					break;
				case 1:
					fragment = new SubscriptionsFragment();
					break;
				case 2:
					fragment = new UploadsFragment();
					break;
				default:
					return null;
			}
			return fragment;
		}
		
		@Override
		public int getItemCount() {
			return 3;
		}
		
	}
	
}
