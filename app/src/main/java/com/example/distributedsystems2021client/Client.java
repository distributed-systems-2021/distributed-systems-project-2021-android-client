package com.example.distributedsystems2021client;


import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.os.Handler;
import android.os.Looper;

import androidx.core.os.HandlerCompat;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import main.event_delivery.Consumer;
import main.event_delivery.Producer;
import main.video.VideoInfo;

public class Client {
	private static final Client singletonInstance = new Client();
	private final Consumer consumer;
	private final ExecutorService executorService;
	private final Handler resultsHandler;
	private final Producer producer;
	private String channelName;
	
	private Client() {
		this.consumer = new Consumer();
		this.producer = new Producer();
		this.executorService = Executors.newFixedThreadPool(4);
		resultsHandler = HandlerCompat.createAsync(Looper.getMainLooper());
		
		initialize();
	}
	
	public static Client getSingletonInstance() {
		return singletonInstance;
	}
	
	public void initialize() {
		executorService.execute(consumer::initialize);
		executorService.execute(producer::initialize);
	}
	
	public void getAvailableSubjects(ResultListener<List<SubjectItem>> resultListener) {
		executorService.execute(() -> {
			List<SubjectItem> availableContent = consumer.getAvailableContent();
			
			Result<List<SubjectItem>> result;
			if(availableContent == null)
				result = Result.createUnsuccessful();
			else
				result = Result.createSuccessful(availableContent);
			
			notifyResult(result, resultListener);
		});
	}
	
	private <T> void notifyResult(final Result<T> result, ResultListener<T> resultListener) {
		resultsHandler.post(() -> resultListener.onResult(result));
	}
	
	public void loadFeedContent(List<SubjectItem> subscriptions, ResultListener<List<VideoItem>> resultListener) {
		executorService.execute(() -> {
			List<VideoItem> videoItems = new ArrayList<>();
			Set<String> existingIDs = new HashSet<>();
			
			for(SubjectItem subscription : subscriptions) {
				List<VideoInfo> videoInfos;
				if(subscription.getType() == SubjectItem.Type.CHANNEL)
					videoInfos = consumer.queryByChannelName(subscription.getName());
				else
					videoInfos = consumer.queryByTopicName(subscription.getName());
				
				if(videoInfos == null)
					continue;
				
				for(VideoInfo videoInfo : videoInfos) {
					if(existingIDs.contains(videoInfo.getId()))
						continue;
					
					existingIDs.add(videoInfo.getId());
					videoItems.add(new VideoItem(videoInfo));
				}
			}
			
			Result<List<VideoItem>> result = Result.createSuccessful(videoItems);
			notifyResult(result, resultListener);
		});
	}
	
	public void downloadVideo(VideoItem videoItem, ResultListener<File> resultListener, Context context) {
		executorService.execute(() -> {
			File tempFile;
			Result<File> result = null;
			try {
				tempFile = createTemporaryFile(context, videoItem.getVideoInfo().getFileExtension());
				boolean successful = consumer.pull(videoItem.getVideoInfo(), tempFile);
				
				result = successful ? Result.createSuccessful(tempFile) : Result.createUnsuccessful();
			}
			catch(IOException e) {
				result = Result.createUnsuccessful(e);
				e.printStackTrace();
			}
			finally {
				notifyResult(result, resultListener);
			}
		});
	}
	
	public void uploadVideo(AssetFileDescriptor file, String fileName, String videoName, String videoTopic, ResultListener<Boolean> resultListener) {
		executorService.execute(() -> {
			try {
				int dotIndex = fileName.indexOf(".");
				String videoExtension = fileName.substring(dotIndex + 1);
				InputStream inputStream = file.createInputStream();
				boolean isSuccess = producer.push(videoTopic, inputStream, videoName, videoExtension, channelName);
				
				file.close();
				
				if(isSuccess) {
					notifyResult(Result.createSuccessful(true), resultListener);
				}
				else {
					notifyResult(Result.createUnsuccessful(), resultListener);
				}
			}
			catch(IOException e) {
				e.printStackTrace();
			}
		});
	}
	
	public void loadChannelVideos(String channelName, ResultListener<List<VideoItem>> resultListener) {
		executorService.execute(() -> {
			
			List<VideoInfo> videoInfos = consumer.queryByChannelName(channelName);
			
			if(videoInfos == null)
				notifyResult(Result.createUnsuccessful(), resultListener);
			
			List<VideoItem> videoItems = videoInfos.stream().map(VideoItem::new).collect(Collectors.toList());
			
			Result<List<VideoItem>> result = Result.createSuccessful(videoItems);
			notifyResult(result, resultListener);
		});
	}
	
	private File createTemporaryFile(Context context, String fileExtension) throws IOException {
		File outputDir = context.getCacheDir();
		return File.createTempFile("temp_file", null, outputDir);
	}
	
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	
}
