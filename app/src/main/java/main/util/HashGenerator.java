package main.util;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashGenerator {
	
	public enum HashAlgorithm {
		SHA1("SHA-1"),
		SHA256("SHA-256"),
		MD5("MD5"),
		JAVA_DEFAULT("");
		
		private final String name;
		
		HashAlgorithm(String name) {
			this.name = name;
		}
		
		public String getName() {
			return name;
		}
		
	}
	
	public static final HashAlgorithm DEFAULT_HASH_ALGORITHM = HashAlgorithm.SHA1;
	
	public static BigInteger hash(String s) {
		return hash(s, DEFAULT_HASH_ALGORITHM);
	}
	
	public static BigInteger hash(String s, HashAlgorithm algorithm) {
		if(algorithm == HashAlgorithm.JAVA_DEFAULT)
			return BigInteger.valueOf(unsignedHash(s));
		
		try {
			MessageDigest messageDigest = MessageDigest.getInstance(algorithm.getName());
			byte[] encodedHash = messageDigest.digest(s.getBytes(StandardCharsets.UTF_8));
			return new BigInteger(1, encodedHash);
		}
		catch(NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		return BigInteger.valueOf(0);
	}
	
	public static int unsignedHash(String s) {
		return s.hashCode() >>> 1;
	}
	
}
