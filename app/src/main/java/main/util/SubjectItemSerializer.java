package main.util;

import com.example.distributedsystems2021client.SubjectItem;

public class SubjectItemSerializer {
	
	public static String toString(SubjectItem subjectItem) {
		return subjectItem.getType().toString() + ":" + subjectItem.getName();
	}
	
	public static SubjectItem fromString(String string) {
		int splitIndex = string.indexOf(":");
		SubjectItem.Type type = SubjectItem.Type.valueOf(string.substring(0, splitIndex));
		String name = string.substring(splitIndex + 1);
		
		return new SubjectItem(name, type);
	}
	
}
