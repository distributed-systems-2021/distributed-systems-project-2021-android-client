package com.example.distributedsystems2021client;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.distributedsystems2021client.databinding.FragmentFeedBinding;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import main.util.SubjectItemSerializer;

public class FeedFragment extends Fragment {
	
	private FragmentFeedBinding binding;
	private List<SubjectItem> subscriptions = new ArrayList<>();
	private SharedPreferences sharedPreferences;
	private String subscriptionsKey;
	
	
	// Required empty public constructor
	public FeedFragment() {}
	
	public static FeedFragment newInstance(String param1, String param2) {
		FeedFragment fragment = new FeedFragment();
		Bundle args = new Bundle();
		fragment.setArguments(args);
		return fragment;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		subscriptionsKey = getResources().getString(R.string.subscriptionsKey);
		sharedPreferences = requireContext().getSharedPreferences(getResources().getString(R.string.sharedPreferencesKey), Context.MODE_PRIVATE);
		
		//loadContent();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		loadContent();	//TODO Add a condition for this
	}
	
	private void loadContent() {
		Set<String> stringSet = sharedPreferences.getStringSet(subscriptionsKey, new HashSet<>());
		subscriptions = stringSet.stream().map(SubjectItemSerializer::fromString).collect(Collectors.toList());
		Client.getSingletonInstance().loadFeedContent(subscriptions, this::onContentLoaded);
	}
	
	private void onContentLoaded(Result<List<VideoItem>> result) {
		if(!result.isSuccessful())
			return;
		
		//TODO Avoid re-initializing the adapter - create a setItems function
		VideoListAdapter adapter = new VideoListAdapter(result.getData(), this::onVideoClicked);
		binding.rcvFeedVideos.setAdapter(adapter);
		
		binding.refresh.setRefreshing(false);
	}
	
	private void onVideoClicked(VideoItem videoItem) {
		Intent intent = new Intent(getContext(), VideoPlaybackActivity.class);
		intent.putExtra(getResources().getString(R.string.videoKey), videoItem);
		startActivity(intent);
	}
	
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		binding = FragmentFeedBinding.inflate(inflater, container, false);
		initializeList();
		binding.refresh.setOnRefreshListener(this::loadContent);
		return binding.getRoot();
	}
	
	private void initializeList() {
		binding.rcvFeedVideos.setLayoutManager(new LinearLayoutManager(getContext()));
	}
	
}
