package com.example.distributedsystems2021client;

import java.util.Objects;

public class SubjectItem {
	
	public enum Type {
		CHANNEL,
		TOPIC
	}
	
	private final String name;
	private final Type type;
	
	public SubjectItem(String name, Type type) {
		this.name = name;
		this.type = type;
	}
	
	public String getName() {
		return name;
	}
	
	public Type getType() {
		return type;
	}
	
	@Override
	public boolean equals(Object o) {
		if(this == o) return true;
		if(o == null || getClass() != o.getClass()) return false;
		SubjectItem that = (SubjectItem) o;
		return Objects.equals(name, that.name) &&
				type == that.type;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(name, type);
	}
	
}
