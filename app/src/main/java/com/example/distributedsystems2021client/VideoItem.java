package com.example.distributedsystems2021client;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import main.video.VideoInfo;

public class VideoItem implements Parcelable {
	
	private final VideoInfo videoInfo;
	public static final Creator<VideoItem> CREATOR = new Creator<VideoItem>() {
		@Override
		public VideoItem createFromParcel(Parcel in) {
			return new VideoItem(in);
		}
		
		@Override
		public VideoItem[] newArray(int size) {
			return new VideoItem[size];
		}
	};
	public static final SimpleDateFormat PARCEL_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	
	public VideoItem(VideoInfo videoInfo) {
		this.videoInfo = videoInfo;
	}
	
	protected VideoItem(Parcel in) {
		this.videoInfo= new VideoInfo(
				in.readString(),
				in.readString(),
				in.readString(),
				in.readString(),
				in.readString(),
				parseDate(in.readString()),
				in.readInt()
		);
	}
	
	private Date parseDate(String dateString) {
		try {
			return PARCEL_DATE_FORMAT.parse(dateString);
		}
		catch(ParseException e) {
			e.printStackTrace();
			return new Date();
		}
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(videoInfo.getId());
		dest.writeString(videoInfo.getVideoName());
		dest.writeString(videoInfo.getFileExtension());
		dest.writeString(videoInfo.getChannelName());
		dest.writeString(videoInfo.getTopicName());
		dest.writeString(PARCEL_DATE_FORMAT.format(videoInfo.getUploadDate()));
		dest.writeInt(videoInfo.getResponsibleBrokerId());
	}
	
	@Override
	public int describeContents() {
		return 0;
	}
	
	public VideoInfo getVideoInfo() {
		return videoInfo;
	}
	
}
