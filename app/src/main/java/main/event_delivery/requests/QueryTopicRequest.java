package main.event_delivery.requests;

public class QueryTopicRequest extends BaseRequest {
	
	private final String topicName;
	
	public QueryTopicRequest(String topicName) {
		this.topicName = topicName;
	}
	
	@Override
	public RequestType getType() {
		return RequestType.QUERY_TOPIC_REQUEST;
	}
	
	public String getTopicName() {
		return topicName;
	}
	
}
