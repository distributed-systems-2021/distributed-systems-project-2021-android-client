package com.example.distributedsystems2021client;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.distributedsystems2021client.databinding.ItemVideoBinding;

import java.text.SimpleDateFormat;
import java.util.List;

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.VideoViewHolder> {
	
	private final List<VideoItem> items;
	private final boolean hideChannelNames;
	private final VideoOnClickListener listener;
	
	public VideoListAdapter(List<VideoItem> items, VideoOnClickListener listener) {
		this.items = items;
		hideChannelNames = false;
		this.listener = listener;
	}
	
	public VideoListAdapter(List<VideoItem> items, boolean hideChannelNames, VideoOnClickListener listener) {
		this.items = items;
		this.hideChannelNames = hideChannelNames;
		this.listener = listener;
	}
	
	@NonNull
	@Override
	public VideoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		ItemVideoBinding binding = ItemVideoBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
		return new VideoViewHolder(binding);
	}
	
	@Override
	public void onBindViewHolder(@NonNull VideoViewHolder holder, int position) {
		holder.onBind(items.get(position));
	}
	
	@Override
	public int getItemCount() {
		return items.size();
	}
	
	
	// Inner classes
	
	class VideoViewHolder extends RecyclerView.ViewHolder {
		
		private ItemVideoBinding binding;
		private VideoItem videoItem;
		public final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
		
		public VideoViewHolder(@NonNull View itemView) {
			super(itemView);
		}
		
		public VideoViewHolder(ItemVideoBinding itemVideoBinding) {
			this(itemVideoBinding.getRoot());
			this.binding = itemVideoBinding;
			this.itemView.setOnClickListener(this::onClick);
		}
		
		public void onBind(VideoItem videoItem) {
			this.videoItem = videoItem;
			
			binding.name.setText(videoItem.getVideoInfo().getVideoName());
			binding.topicName.setText(videoItem.getVideoInfo().getTopicName());
			binding.channelName.setText(videoItem.getVideoInfo().getChannelName());
			binding.date.setText(DATE_FORMAT.format(videoItem.getVideoInfo().getUploadDate()));
			
			if(hideChannelNames)
				binding.channelName.setVisibility(View.GONE);
		}
		
		private void onClick(View view) {
			listener.onClick(videoItem);
		}
		
	}
	
	interface VideoOnClickListener {
		
		void onClick(VideoItem videoItem);
		
	}
	
}
