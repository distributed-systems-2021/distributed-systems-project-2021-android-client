package main.event_delivery.requests;

import java.io.Serializable;
import java.util.Arrays;

public class FileChunk implements Serializable {
	private final byte[] bytes;
	private final boolean isTerminating;
	
	public static final int DEFAULT_SIZE = 4096;
	
	public static final FileChunk TERMINATING_CHUNK = new FileChunk(null, true);
	
	public FileChunk(byte[] bytes, int actualSize) {
		this.bytes = Arrays.copyOfRange(bytes, 0, actualSize);
		isTerminating = false;
	}
	
	private FileChunk(byte[] bytes, boolean isTerminating) {
		this.bytes = bytes;
		this.isTerminating = isTerminating;
	}
	
	public byte[] getBytes() {
		return bytes;
	}
	
	public int getSize() {
		if(bytes == null)
			return -1;
		
		return bytes.length;
	}
	
	public boolean isTerminating() {
		return isTerminating;
	}
	
}
