package com.example.distributedsystems2021client;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.distributedsystems2021client.databinding.FragmentSubscriptionsBinding;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import main.util.SubjectItemSerializer;

public class SubscriptionsFragment extends Fragment {
	
	private FragmentSubscriptionsBinding binding;
	private List<SubjectItem> subscriptions = new ArrayList<>();
	private SubjectListAdapter existingSubscriptionsAdapter;
	private SubjectListAdapter availableSubjectsAdapter;
	private SharedPreferences sharedPreferences;
	private String subscriptionsKey;
	
	private SharedPreferences.Editor preferencesEditor = null;
	
	
	public SubscriptionsFragment() {
		// Required empty public constructor
	}
	
	public static SubscriptionsFragment newInstance() {
		SubscriptionsFragment fragment = new SubscriptionsFragment();
		Bundle args = new Bundle();
		fragment.setArguments(args);
		return fragment;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		subscriptionsKey = getContext().getString(R.string.subscriptionsKey);
		sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.sharedPreferencesKey), Context.MODE_PRIVATE);
		
		loadSubscriptions();
	}
	
	private void loadSubscriptions() {
		Set<String> stringSet = sharedPreferences.getStringSet(subscriptionsKey, new HashSet<>());
		subscriptions = stringSet.stream().map(SubjectItemSerializer::fromString).collect(Collectors.toList());
	}
	
	private void saveSubscriptions() {
		Set<String> stringSet = subscriptions.stream().map(SubjectItemSerializer::toString).collect(Collectors.toSet());
		getPreferencesEditor().putStringSet(subscriptionsKey, stringSet);
		getPreferencesEditor().apply();
	}
	
	private SharedPreferences.Editor getPreferencesEditor() {
		if(preferencesEditor == null)
			preferencesEditor = sharedPreferences.edit();
		
		return preferencesEditor;
	}
	
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		binding = FragmentSubscriptionsBinding.inflate(inflater, container, false);
		
		configureControls();
		configureListeners();
		
		return binding.getRoot();
	}
	
	private void configureControls() {
		binding.rcvSubscriptions.setLayoutManager(new LinearLayoutManager(getContext()));
		binding.rcvAvailableSubjects.setLayoutManager(new LinearLayoutManager(getContext()));
		
		availableSubjectsAdapter = new SubjectListAdapter(new ArrayList<>(), this::addSubscription, false);
		binding.rcvAvailableSubjects.setAdapter(availableSubjectsAdapter);
		
		existingSubscriptionsAdapter = new SubjectListAdapter(subscriptions, this::removeSubscription,true);
		binding.rcvSubscriptions.setAdapter(existingSubscriptionsAdapter);
		
	}
	
	private void configureListeners() {
		binding.addSubscriptionButton.setOnClickListener(this::showAvailableSubjects);
		binding.submitButton.setOnClickListener(this::showExistingSubscriptions);
	}
	
	private void showExistingSubscriptions(View view) {
		existingSubscriptionsAdapter.setItems(subscriptions);
		
		binding.grpExistingSubscriptions.setVisibility(View.VISIBLE);
		binding.grpAvailableSubjects.setVisibility(View.GONE);
	}
	
	private void showAvailableSubjects(View view) {
		getAvailableContent();
		binding.grpExistingSubscriptions.setVisibility(View.GONE);
		binding.grpAvailableSubjects.setVisibility(View.VISIBLE);
	}
	
	private void getAvailableContent() {
		Client.getSingletonInstance().getAvailableSubjects(this::onAvailableContentReceived);
	}
	
	private void onAvailableContentReceived(Result<List<SubjectItem>> result) {
		if(!result.isSuccessful())
			return;
		
		List<SubjectItem> data = result.getData();
		data.removeAll(subscriptions);
		availableSubjectsAdapter.setItems(data);
	}
	
	private void addSubscription(SubjectItem subjectItem) {
		subscriptions.add(subjectItem);
		saveSubscriptions();
	}
	
	private void removeSubscription(SubjectItem subjectItem) {
		subscriptions.remove(subjectItem);
		saveSubscriptions();
	}
	
}
