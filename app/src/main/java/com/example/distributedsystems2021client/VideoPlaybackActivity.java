package com.example.distributedsystems2021client;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;

import com.example.distributedsystems2021client.databinding.ActivityVideoPlaybackBinding;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.SimpleExoPlayer;

import java.io.File;

public class VideoPlaybackActivity extends AppCompatActivity {
	
	private ActivityVideoPlaybackBinding binding;
	private SimpleExoPlayer exoPlayer;
	private static final String EXO_PLAYER_PLAYING_ON_PAUSE = "EXO_PLAYER_PLAYING_ON_PAUSE";
	private SharedPreferences sharedPreferences;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = ActivityVideoPlaybackBinding.inflate(getLayoutInflater());
		setContentView(binding.getRoot());
		
		sharedPreferences = getSharedPreferences(getResources().getString(R.string.sharedPreferencesKey), MODE_PRIVATE);
		
		VideoItem video = getIntent().getParcelableExtra(getResources().getString(R.string.videoKey));
		
		exoPlayer = new SimpleExoPlayer.Builder(this).build();
		binding.player.setPlayer(exoPlayer);
		
		Client.getSingletonInstance().downloadVideo(video, this::onFileDownloaded, this);
		
		exoPlayer.prepare();
	}
	
	private void onFileDownloaded(Result<File> result) {
		if(!result.isSuccessful())
			return;
		
		//todo null check ?
		
		MediaItem mediaItem = MediaItem.fromUri(result.getData().getAbsolutePath());
		exoPlayer.setMediaItem(mediaItem);
		exoPlayer.play();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		exoPlayer.stop();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		exoPlayer.stop();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		sharedPreferences.edit().putBoolean(EXO_PLAYER_PLAYING_ON_PAUSE, exoPlayer.isPlaying()).apply();
		exoPlayer.pause();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if(sharedPreferences.getBoolean(EXO_PLAYER_PLAYING_ON_PAUSE, Boolean.FALSE))
			exoPlayer.play();
	}
	
}
