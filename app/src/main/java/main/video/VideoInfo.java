package main.video;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.Date;

public class VideoInfo implements Serializable {
	
	private static final long serialVersionUID = 7623199017158219704L;
	
	private final String id;
	private String videoName;
	private final String fileExtension;
	private String channelName;
	private String topicName;
	private final Date uploadDate;
	private final int responsibleBrokerId;
	
	public static final VideoInfo INVALID = null;
	
	public VideoInfo(String id, String videoName, String fileExtension, String channelName, String topicName, Date uploadDate, int responsibleBrokerId) {
		this.id = id;
		this.videoName = videoName;
		this.fileExtension = fileExtension;
		this.channelName = channelName;
		this.topicName = topicName;
		this.uploadDate = uploadDate;
		this.responsibleBrokerId = responsibleBrokerId;
	}
	
	public VideoInfo() {
		this(null, null, null, null, null, null, -1);
	}
	
	public String getId() {
		return id;
	}
	
	public String getVideoName() {
		return videoName;
	}
	
	public void setVideoName(String videoName) {
		this.videoName = videoName;
	}
	
	public String getFileExtension() {
		return fileExtension;
	}
	
	public String getChannelName() {
		return channelName;
	}
	
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	
	public String getTopicName() {
		return topicName;
	}
	
	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}
	
	public Date getUploadDate() {
		return uploadDate;
	}
	
	public int getResponsibleBrokerId() {
		return responsibleBrokerId;
	}
	
	@NonNull
	@Override
	public String toString() {
		return "VideoInfo{" +
				"id='" + id + '\'' +
				", videoName='" + videoName + '\'' +
				", fileExtension='" + fileExtension + '\'' +
				", channelName='" + channelName + '\'' +
				", topicName='" + topicName + '\'' +
				", uploadDate='" + uploadDate + '\'' +
				", responsibleBrokerId='" + responsibleBrokerId + '\'' +
				'}';
	}
	
}
