package main.event_delivery.requests;

public class DiscoveryRequest extends BaseRequest {
	
	@Override
	public RequestType getType() {
		return RequestType.DISCOVERY_REQUEST;
	}
	
}
