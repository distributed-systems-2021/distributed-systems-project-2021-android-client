package com.example.distributedsystems2021client;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.distributedsystems2021client.databinding.ItemSubjectBinding;

import java.util.ArrayList;
import java.util.List;

public class SubjectListAdapter extends RecyclerView.Adapter<SubjectListAdapter.SubjectViewHolder> {
	
	private List<SubjectItem> items;
	private final boolean showingExistingSubscriptions;
	private final SubjectOnClickListener listener;
	
	public SubjectListAdapter(List<SubjectItem> items, SubjectOnClickListener listener, boolean showingExistingSubscriptions) {
		this.items = items;
		this.showingExistingSubscriptions = showingExistingSubscriptions;
		this.listener = listener;
	}
	
	@NonNull
	@Override
	public SubjectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		ItemSubjectBinding binding = ItemSubjectBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
		return new SubjectViewHolder(binding);
	}
	
	@Override
	public void onBindViewHolder(@NonNull SubjectViewHolder holder, int position) {
		holder.onBind(items.get(position));
	}
	
	@Override
	public int getItemCount() {
		return items.size();
	}
	
	public void setItems(List<SubjectItem> items) {
		this.items = new ArrayList<>(items);
		notifyDataSetChanged();
	}
	
	// Inner classes
	
	class SubjectViewHolder extends RecyclerView.ViewHolder {
		
		SubjectItem item;
		ItemSubjectBinding binding;
		
		public SubjectViewHolder(@NonNull View itemView) {
			super(itemView);
		}
		
		public SubjectViewHolder(ItemSubjectBinding itemChannelTopicBinding) {
			this(itemChannelTopicBinding.getRoot());
			this.binding = itemChannelTopicBinding;
			this.binding.btnAction.setOnClickListener(this::onClick);
		}
		
		public void onBind(SubjectItem subjectItem) {
			this.item = subjectItem;
			
			binding.name.setText(subjectItem.getName());
			
			if(subjectItem.getType() == SubjectItem.Type.CHANNEL) {
				binding.channelIcon.setVisibility(View.VISIBLE);
				binding.topicIcon.setVisibility(View.INVISIBLE);
			}
			else {
				binding.channelIcon.setVisibility(View.INVISIBLE);
				binding.topicIcon.setVisibility(View.VISIBLE);
			}
			
			binding.btnAction.setText(itemView.getContext().getString( showingExistingSubscriptions ? R.string.unsubscribeText : R.string.subscribeText ));
		}
		
		public void onClick(View v) {
			listener.onClick(item);
			items.remove(item);
			notifyItemRemoved(getAdapterPosition());
		}
		
	}
	
	interface SubjectOnClickListener {
		
		void onClick(SubjectItem item);
		
	}
	
}
