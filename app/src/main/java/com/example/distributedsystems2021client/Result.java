package com.example.distributedsystems2021client;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class Result<T> {
	
	private final T data;
	private final Exception exception;
	private final boolean isSuccessful;
	
	private Result(T data, Exception e, boolean isSuccessful) {
		this.data = data;
		this.exception = null;
		this.isSuccessful = isSuccessful;
	}
	
	public static <T> Result<T> createSuccessful(@NonNull T data) {
		return new Result<>(data, null, true);
	}
	
	public static <T> Result<T> createUnsuccessful(Exception e) {
		return new Result<>(null, e, false);
	}
	
	public static <T> Result<T> createUnsuccessful() {
		return new Result<>(null, null, false);
	}
	
	public T getData() {
		return data;
	}
	
	@Nullable
	public Exception getException() {
		return exception;
	}
	
	public boolean isSuccessful() {
		return isSuccessful;
	}
	
}
