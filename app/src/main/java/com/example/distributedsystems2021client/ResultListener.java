package com.example.distributedsystems2021client;

public interface ResultListener<T> {
	
	void onResult(Result<T> result);
	
}
