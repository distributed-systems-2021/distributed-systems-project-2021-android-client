package com.example.distributedsystems2021client;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.distributedsystems2021client.databinding.FragmentUploadsBinding;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.ArrayList;
import java.util.List;

public class UploadsFragment extends Fragment {
	
	FragmentUploadsBinding binding;
	private SharedPreferences sharedPreferences;
	private String channelName;
	private DialogInterface.OnClickListener onConfirmListener;
	private DialogInterface.OnClickListener onCancelListener;
	
	public UploadsFragment() {
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sharedPreferences = getContext().getSharedPreferences(getContext().getString(R.string.sharedPreferencesKey), Context.MODE_PRIVATE);
	}
	
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		binding = FragmentUploadsBinding.inflate(inflater, container, false);
		initializeList();
		binding.refresh.setOnRefreshListener(this::loadContent);

		binding.uploadButton.setOnClickListener(this::onUploadClicked);
		
		channelName = sharedPreferences.getString(getString(R.string.channelNameKey), null);
		if(channelName != null) {
			binding.channelName.setText(channelName);
			Client.getSingletonInstance().setChannelName(channelName);
		}
		return binding.getRoot();
	}
	
	private void onUploadClicked(View view) {
		if(channelName == null) {
			handleNoChannelName();
			
			return;
		}
		Intent intent = new Intent(getContext(), VideoUploadActivity.class);
		startActivity(intent);
	}
	
	private void handleNoChannelName() {
		final EditText channelNameInput = getDialogEditText();
		
		onConfirmListener = (dialogInterface, i) -> {
			String input = channelNameInput.getText().toString().trim();
			if(input.length() == 0) {
				// TODO consider using Snackbars instead of Toasts
				Toast.makeText(getContext(), R.string.channel_name_error, Toast.LENGTH_SHORT).show();
				return;
			}
			saveChannelName(input);
			Intent intent = new Intent(getContext(), VideoUploadActivity.class);
			startActivity(intent);
		};
		
		onCancelListener = (dialogInterface, i) -> dialogInterface.cancel();
		
		new MaterialAlertDialogBuilder(getContext())
				.setTitle(getString(R.string.choose_name_dialog_title))
				.setView(channelNameInput)
				.setNegativeButton(getString(R.string.cancel), onCancelListener)
				.setPositiveButton(getString(R.string.confirm), onConfirmListener)
				.show();
	}
	
	private EditText getDialogEditText() {
		final EditText channelNameInput = new EditText(getContext());
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.MATCH_PARENT);
		channelNameInput.setLayoutParams(lp);
		return channelNameInput;
	}
	
	private void saveChannelName(String input) {
		channelName = input;
		binding.channelName.setText(channelName);
		Client.getSingletonInstance().setChannelName(input);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(getString(R.string.channelNameKey), input);
		editor.apply();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		loadContent();
	}

	private void loadContent() {
		if(channelName == null) {
			return;
		}
		// TODO does this also need a swipe to refresh
		Client.getSingletonInstance().loadChannelVideos(channelName, this::onVideosLoaded);
	}

	private void onVideosLoaded(Result<List<VideoItem>> listResult) {
		if(!listResult.isSuccessful()) {
			//TODO handle error
		}
		// TODO avoid creating a new adapter each time
		VideoListAdapter adapter = new VideoListAdapter(listResult.getData(), true, this::onVideoClicked);
		binding.rcvUploads.setAdapter(adapter);
		binding.refresh.setRefreshing(false);
	}
	
	private void initializeList() {
		List<VideoItem> feedVideos = new ArrayList<>();
		
		VideoListAdapter adapter = new VideoListAdapter(feedVideos, true, this::onVideoClicked);
		
		binding.rcvUploads.setLayoutManager(new LinearLayoutManager(getContext()));
		binding.rcvUploads.setAdapter(adapter);
	}
	
	private void onVideoClicked(VideoItem videoItem) {
		Intent intent = new Intent(getContext(), VideoPlaybackActivity.class);
		intent.putExtra(getResources().getString(R.string.videoKey), videoItem);
		startActivity(intent);
	}
	
}
