package com.example.distributedsystems2021client;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.distributedsystems2021client.databinding.ActivityVideoUploadBinding;
import com.google.android.material.snackbar.Snackbar;

import java.io.FileNotFoundException;

import static android.provider.MediaStore.MediaColumns.DISPLAY_NAME;


public class VideoUploadActivity extends AppCompatActivity {
	private static final int PERMISSION_REQUEST_CAMERA = 42;
	private ActivityVideoUploadBinding binding;
	private Uri selectedFileUri;
	private ActivityResultLauncher<Intent> fileLauncher;
	private ActivityResultLauncher<Intent> recordVideoLauncher;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = ActivityVideoUploadBinding.inflate(getLayoutInflater());
		setContentView(binding.getRoot());
		
		binding.btnUploadFromFile.setOnClickListener(this::onUploadFromFileClicked);
		binding.btnRecordNew.setOnClickListener(this::onRecordNewClicked);
		binding.btnSubmit.setOnClickListener(this::onSubmitClicked);
		
		fileLauncher = getFileLauncher();
		recordVideoLauncher = getRecordVideoLauncher();
	}
	
	private void onRecordNewClicked(View view) {
		if(checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
			recordVideo();
		else
			requestCameraPermission();
	}
	
	private void recordVideo() {
		Intent videoCaptureIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
		recordVideoLauncher.launch(videoCaptureIntent);
	}
	
	private void onSubmitClicked(View view) {
		String videoName = binding.etName.getText().toString().trim();
		String videoTopic = binding.etTopic.getText().toString().trim();
		
		if(videoName.length() == 0 || videoTopic.length() == 0) {
			Toast.makeText(this, R.string.upload_form_error, Toast.LENGTH_LONG).show();
			return;
		}
		
		binding.btnSubmit.setEnabled(false);
		// selectedFileUri should never be null here
		try {
			AssetFileDescriptor fd = getContentResolver().openAssetFileDescriptor(selectedFileUri, "r");
			String fileName = getFileName(selectedFileUri);
			Client.getSingletonInstance().uploadVideo(fd, fileName, videoName, videoTopic, this::onUploadFinished);
			
		}
		catch(FileNotFoundException e) {
			//TODO handle error
			e.printStackTrace();
		}
		
	}
	
	private void onUploadFromFileClicked(View view) {
		Intent fileUploadIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
		fileUploadIntent.setType("video/*");
		fileLauncher.launch(fileUploadIntent);
	}
	
	public ActivityResultLauncher<Intent> getFileLauncher() {
		return registerForActivityResult(
				new ActivityResultContracts.StartActivityForResult(),
				result -> {
					//TODO handle error result codes
					if(result.getResultCode() == Activity.RESULT_OK) {
						Intent data = result.getData();
						// TODO is this ever null?
						selectedFileUri = data.getData();
						showSecondStepForm();
					}
				});
	}
	
	public ActivityResultLauncher<Intent> getRecordVideoLauncher() {
		return registerForActivityResult(
				new ActivityResultContracts.StartActivityForResult(),
				result -> {
					//TODO handle error result codes
					if(result.getResultCode() == Activity.RESULT_OK) {
						Intent data = result.getData();
						// TODO is this ever null?
						selectedFileUri = data.getData();
						showSecondStepForm();
					}
				});
	}
	
	private void showSecondStepForm() {
		binding.groupStep2.setVisibility(View.VISIBLE);
		binding.groupStep1.setVisibility(View.GONE);
	}
	
	private String getFileName(Uri fileUri) {
		Cursor cursor = getContentResolver().query(fileUri, null, null, null, null);
		cursor.moveToFirst();
		String fileName = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
		cursor.close();
		return fileName;
	}
	
	private void onUploadFinished(Result<Boolean> result) {
		if(result.isSuccessful())
			Toast.makeText(this, R.string.upload_success, Toast.LENGTH_SHORT).show();
		else
			Toast.makeText(this, R.string.upload_fail, Toast.LENGTH_LONG).show();
		
		finish();
	}
	
	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		// BEGIN_INCLUDE(onRequestPermissionsResult)
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if(requestCode == PERMISSION_REQUEST_CAMERA) {
			// Request for camera permission.
			if(grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				// Permission has been granted. Start camera preview Activity.
				Snackbar.make(binding.getRoot(), R.string.camera_permission_granted, Snackbar.LENGTH_SHORT).show();
				recordVideo();
			}
			else {
				// Permission request was denied.
				Snackbar.make(binding.getRoot(), R.string.camera_permission_denied, Snackbar.LENGTH_SHORT).show();
			}
		}
	}
	
	private void requestCameraPermission() {
		//https://developer.android.com/training/permissions/requesting
		//https://github.com/android/permissions-samples/blob/main/RuntimePermissionsBasic/Application/src/main/java/com/example/android/basicpermissions/MainActivity.java
		// Permission has not been granted and must be requested.
		if(shouldShowRequestPermissionRationale(Manifest.permission.CAMERA))
			// Provide an additional rationale to the user if the permission was not granted
			// and the user would benefit from additional context for the use of the permission.
			// Display a SnackBar with cda button to request the missing permission.
			Snackbar.make(binding.getRoot(), R.string.camera_access_required, Snackbar.LENGTH_INDEFINITE)
					.setAction(R.string.ok, view -> requestPermissions(
						new String[]{Manifest.permission.CAMERA},
						PERMISSION_REQUEST_CAMERA))
					.show();
		else
			// Request the permission - the result will be received in onRequestPermissionResult()
			requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CAMERA);
	}
	
}
