package main.event_delivery.broker;

import main.util.HashGenerator;

import java.io.Serializable;
import java.math.BigInteger;

public class BrokerTopology implements Serializable {
	
	private static final long serialVersionUID = -1152553692028966212L;
	private final int id;
	private final String ip;
	private final int portForProducers;
	private final int portForConsumers;
	private final int portForBrokers;
	private final BigInteger hashcode;
	
	public BrokerTopology(int id, String ip, int portForProducers, int portForConsumers, int portForBrokers) {
		this.id = id;
		this.ip = ip;
		this.portForProducers = portForProducers;
		this.portForConsumers = portForConsumers;
		this.portForBrokers = portForBrokers;
		this.hashcode = HashGenerator.hash(ip + portForProducers + portForConsumers + portForBrokers);
	}
	
	public int getId() {
		return id;
	}
	
	public String getIp() {
		return ip;
	}
	
	public int getPortForProducers() {
		return portForProducers;
	}
	
	public int getPortForConsumers() {
		return portForConsumers;
	}
	
	public int getPortForBrokers() {
		return portForBrokers;
	}
	
	public BigInteger getHashcode() {
		return hashcode;
	}
	
}
