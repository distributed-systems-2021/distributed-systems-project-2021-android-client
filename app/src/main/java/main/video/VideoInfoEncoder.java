package main.video;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class VideoInfoEncoder {
	
	public static final String DEFAULT_DELIMITER = "\t\t";
	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	
	public static final Date BACKUP_DATE = new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime();
	
	public static String encode(VideoInfo videoInfo) {
		return encode(videoInfo, DEFAULT_DELIMITER);
	}
	
	public static String encode(VideoInfo videoInfo, String delimiter) {
		return videoInfo.getId() + delimiter +
				videoInfo.getVideoName() + delimiter +
				videoInfo.getFileExtension() + delimiter +
				videoInfo.getChannelName() + delimiter +
				videoInfo.getTopicName() + delimiter +
				dateToString(videoInfo.getUploadDate()) + delimiter +
				videoInfo.getResponsibleBrokerId();
	}
	
	public static VideoInfo decode(String text) {
		return decode(text, DEFAULT_DELIMITER);
	}
	
	public static VideoInfo decode(String text, String delimiter) {
		String[] infoParts = text.split(delimiter);
		
		return new VideoInfo(
				infoParts[0],
				infoParts[1],
				infoParts[2],
				infoParts[3],
				infoParts[4],
				stringToDate(infoParts[5]),
				Integer.parseInt(infoParts[6]));
	}
	
	public static String dateToString(Date date) {
		return DATE_FORMAT.format(date);
	}
	
	public static Date stringToDate(String string) {
		try {
			return DATE_FORMAT.parse(string);
		}
		catch(ParseException e) {
			e.printStackTrace();
			return BACKUP_DATE;
		}
	}
	
}
