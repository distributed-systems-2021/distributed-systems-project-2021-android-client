package main.util;

import main.event_delivery.requests.FileChunk;

import java.io.*;

public class StreamUtils {
	
	@Deprecated
	@SuppressWarnings("deprecation")
	public static void copy(InputStream inputStream, OutputStream outputStream) throws IOException {
		copy(inputStream, outputStream, FileChunk.DEFAULT_SIZE);
	}
	
	@Deprecated
	public static void copy(InputStream inputStream, OutputStream outputStream, int chunkSize) throws IOException {
		int count;
		byte[] buffer = new byte[chunkSize];
		while((count = inputStream.read(buffer)) >= 0) {
			outputStream.write(buffer, 0, count);
		}
	}
	
	public static void copyChunks(ObjectInputStream objectInputStream, OutputStream outputStream) throws IOException, ClassNotFoundException {
		FileChunk currentChunk;
		while(true) {
			currentChunk = (FileChunk) objectInputStream.readObject();
			if(currentChunk.isTerminating())
				break;
			outputStream.write(currentChunk.getBytes(), 0, currentChunk.getSize());
		}
	}
	
	public static void copyChunks(InputStream inputStream, ObjectOutputStream objectOutputStream) throws IOException {
		copyChunks(inputStream, objectOutputStream, FileChunk.DEFAULT_SIZE);
	}
	
	public static void copyChunks(InputStream inputStream, ObjectOutputStream objectOutputStream, int chunkSize) throws IOException {
		int byteCount;
		byte[] buffer = new byte[chunkSize];
		while((byteCount = inputStream.read(buffer)) > 0) {
			objectOutputStream.writeObject(new FileChunk(buffer, byteCount));
		}
		objectOutputStream.writeObject(FileChunk.TERMINATING_CHUNK);
	}
	
}
