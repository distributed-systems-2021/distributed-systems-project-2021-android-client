package main.event_delivery;

import com.example.distributedsystems2021client.SubjectItem;

import main.event_delivery.broker.BrokerContent;
import main.event_delivery.broker.BrokerTopology;
import main.event_delivery.requests.DiscoveryRequest;
import main.network.SocketWrapper;
import main.util.StreamUtils;
import main.video.VideoInfo;
import main.util.Logger;
import main.event_delivery.requests.PullRequest;
import main.event_delivery.requests.QueryChannelRequest;
import main.event_delivery.requests.QueryTopicRequest;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class Consumer extends ClientNode {
	
	public static final String STORAGE_DIRECTORY_NAME = "Downloads";
	
	public Consumer() {
		logger = new Logger("Consumer");
	}
	
	@Override
	public void initialize() {
		storageDirectoryName = STORAGE_DIRECTORY_NAME;
		createStorageDirectory();
		sendDiscoveryRequest();
	}
	
	public List<SubjectItem> getAvailableContent() {
		logger.info("Sending discovery request");
		
		SocketWrapper connection = connectToBroker(standardBroker);
		
		try {
			ObjectOutputStream objectOutputStream = connection.getObjectOutputStream();
			ObjectInputStream objectInputStream = connection.getObjectInputStream();
			objectOutputStream.writeObject(new DiscoveryRequest());
			
			@SuppressWarnings("unchecked")
			Map<BrokerTopology, BrokerContent> mapOfContents = (Map<BrokerTopology, BrokerContent>) objectInputStream.readObject();
			
			logger.startGroup();
			logger.info("Received info on the contents of Brokers");
			for(BrokerTopology broker : mapOfContents.keySet()) {
				logger.info("Broker %d: %s", broker.getId(), mapOfContents.get(broker).toString());
			}
			logger.endGroup();
			
			return getSubjectItems(mapOfContents);
		}
		catch(IOException | ClassNotFoundException e) {
			logger.exception(e);
		}
		
		return null;
	}
	
	protected List<SubjectItem> getSubjectItems(Map<BrokerTopology, BrokerContent> mapOfContents) {
		Set<String> topicNames = new HashSet<>();
		Set<String> channelNames = new HashSet<>();
		
		for(BrokerContent content : mapOfContents.values()) {
			topicNames.addAll(content.getTopicNames());
			channelNames.addAll(content.getChannelNames());
		}
		
		List<SubjectItem> subjectItems = new ArrayList<>();
		subjectItems.addAll(topicNames.stream().map(t -> new SubjectItem(t, SubjectItem.Type.TOPIC)).collect(Collectors.toList()));
		subjectItems.addAll(channelNames.stream().map(t -> new SubjectItem(t, SubjectItem.Type.CHANNEL)).collect(Collectors.toList()));
		
		return subjectItems;
	}
	
	public List<VideoInfo> queryByChannelName(String channelName) {
		SocketWrapper wrapper = connectToProperBrokerExplicit(channelName);
		if(wrapper == null) return null;
		
		try {
			ObjectOutputStream objectOutputStream = wrapper.getObjectOutputStream();
			ObjectInputStream objectInputStream = wrapper.getObjectInputStream();
			QueryChannelRequest request = new QueryChannelRequest(channelName);
			objectOutputStream.writeObject(request);
			
			@SuppressWarnings("unchecked")
			List<VideoInfo> videoInfos = (List<VideoInfo>) objectInputStream.readObject();
			logger.info("Query results for channel \"" + channelName + "\"");
			for(VideoInfo videoInfo : videoInfos) {
				logger.info(videoInfo.toString());
			}
			return videoInfos;
		}
		catch(IOException | ClassNotFoundException e) {
			logger.exception(e);
		}
		
		return null;
	}
	
	public List<VideoInfo> queryByTopicName(String topicName) {
		SocketWrapper wrapper = connectToProperBrokerExplicit(topicName);
		if(wrapper == null) return null;
		
		try {
			ObjectOutputStream objectOutputStream = wrapper.getObjectOutputStream();
			ObjectInputStream objectInputStream = wrapper.getObjectInputStream();
			QueryTopicRequest request = new QueryTopicRequest(topicName);
			objectOutputStream.writeObject(request);
			
			@SuppressWarnings("unchecked")
			List<VideoInfo> videoInfos = (List<VideoInfo>) objectInputStream.readObject();
			logger.info("Query results for topic \"" + topicName + "\"");
			
			for(VideoInfo videoInfo : videoInfos) {
				logger.info(videoInfo.toString());
			}
			
			return videoInfos;
		}
		catch(IOException | ClassNotFoundException e) {
			logger.exception(e);
		}
		
		return null;
	}
	
	public boolean pull(VideoInfo targetVideo, File destinationFile) {
		BrokerTopology targetBrokerTopology = getBrokerFor(targetVideo);
		SocketWrapper wrapper = connectToBroker(targetBrokerTopology);
		if(wrapper == null) return false;
		
		try {
			ObjectOutputStream objectOutputStream = wrapper.getObjectOutputStream();
			ObjectInputStream objectInputStream = wrapper.getObjectInputStream();
			PullRequest request = new PullRequest(targetVideo.getId());
			objectOutputStream.writeObject(request);
			
			VideoInfo videoInfo = (VideoInfo) objectInputStream.readObject();
			if(videoInfo == VideoInfo.INVALID) {
				logger.info("The requested video does not exist");
				return false;
			}
			
			FileOutputStream fileOutputStream = new FileOutputStream(destinationFile);
			StreamUtils.copyChunks(objectInputStream, fileOutputStream);
			fileOutputStream.close();
			return true;
		}
		catch(IOException | ClassNotFoundException e) {
			logger.exception(e);
		}
		
		return false;
	}
	
	private File getFileSaveLocation(VideoInfo videoInfo) {
		//return Path.of(storageDirectoryName, videoInfo.getVideoName() + "." + videoInfo.getFileExtension()).toFile();
		// TODO
		return null;
	}
	
	@Override
	protected BrokerTopology getBrokerFor(VideoInfo targetVideo) {
		return knownBrokers.get(targetVideo.getResponsibleBrokerId());
	}
	
	@Override
	protected int getCorrespondingPort(BrokerTopology topology) {
		return topology.getPortForConsumers();
	}
	
}
