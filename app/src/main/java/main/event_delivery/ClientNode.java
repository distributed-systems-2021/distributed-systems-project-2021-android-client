package main.event_delivery;

import main.event_delivery.broker.BrokerTopology;
import main.event_delivery.broker.BrokerContent;
import main.event_delivery.requests.DisconnectRequest;
import main.event_delivery.requests.DiscoveryRequest;
import main.network.SocketWrapper;
import main.video.VideoInfo;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public abstract class ClientNode extends Node {
	
	public static final String IP = "192.168.2.100";
	
	protected final Map<Integer, BrokerTopology> knownBrokers = new HashMap<>();
	protected final BrokerTopology standardBroker = new BrokerTopology(1, IP, 5001, 6001, 7001);	//Temporary
	protected final Map<Integer, SocketWrapper> existingConnections = new HashMap<>();
	
	protected void sendDiscoveryRequest() {
		logger.info("Sending discovery request");
		
		SocketWrapper connection = connectToBroker(standardBroker);
		
		try {
			ObjectOutputStream objectOutputStream = connection.getObjectOutputStream();
			ObjectInputStream objectInputStream = connection.getObjectInputStream();
			objectOutputStream.writeObject(new DiscoveryRequest());
			
			@SuppressWarnings("unchecked")
			Map<BrokerTopology, BrokerContent> mapOfContents = (Map<BrokerTopology, BrokerContent>) objectInputStream.readObject();
			
			knownBrokers.clear();
			for(BrokerTopology brokerTopology : mapOfContents.keySet()) {
				knownBrokers.put(brokerTopology.getId(), brokerTopology);
			}
			
		}
		catch(IOException | ClassNotFoundException e) {
			logger.exception(e);
		}
	}
	
	protected SocketWrapper connectToBroker(BrokerTopology targetBrokerTopology) {
		if(existingConnections.containsKey(targetBrokerTopology.getId())) {
			logger.debug("Using existing connection to broker %d", targetBrokerTopology.getId());
			return existingConnections.get(targetBrokerTopology.getId());
		}
		
		try {
			logger.debug("Creating a new connection to broker %d", targetBrokerTopology.getId());
			Socket socket = new Socket(targetBrokerTopology.getIp(), getCorrespondingPort(targetBrokerTopology));
			logger.info("Connected to broker %d", targetBrokerTopology.getId());
			SocketWrapper connection = new SocketWrapper(socket);
			existingConnections.put(targetBrokerTopology.getId(), connection);
			
			return connection;
		}
		catch(IOException e) {
			logger.exception(e);
			return null;
		}
	}
	
	protected abstract BrokerTopology getBrokerFor(VideoInfo targetVideo);
	
	protected SocketWrapper connectToProperBrokerExplicit(String diacriticName) {
		BrokerTopology targetBrokerTopology = getTargetBroker(diacriticName, knownBrokers);
		return connectToBroker(targetBrokerTopology);
	}
	
	protected abstract int getCorrespondingPort(BrokerTopology topology);
	
	public void disconnect() {
		for(SocketWrapper connection : existingConnections.values()) {
			logger.info("Sending a disconnect request");
			try {
				ObjectOutputStream objectOutputStream = connection.getObjectOutputStream();
				objectOutputStream.writeObject(new DisconnectRequest());
				connection.close();
			}
			catch(IOException e) {
				logger.exception(e);
			}
		}
	}
	
}
