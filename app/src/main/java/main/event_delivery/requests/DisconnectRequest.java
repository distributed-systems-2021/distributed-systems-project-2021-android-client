package main.event_delivery.requests;

public class DisconnectRequest extends BaseRequest {
	
	@Override
	public RequestType getType() {
		return RequestType.DISCONNECT_REQUEST;
	}
	
}
